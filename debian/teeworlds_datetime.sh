#!/bin/sh
# Helper script to show timestamps in log files that can be parsed by tools
# like fail2ban. Requires the installation of gawk.
set -e

exec /usr/bin/gawk 'BEGIN { FS="]"; OFS=FS } /^\[.*\]/ { $1="["strftime("%F %T", systime()); print; fflush() }'
